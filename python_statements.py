

#Assignment Statement
salary = 12000
name = 'Ravi K'

#Loop statements
for i in range(10):
    print(i) 

# Conditional Statements
salary = 1300

if salary == 130000:
    print('Your salary is good')

#Pass Statement
def get_user_name():
    pass

get_user_name()

# del(delete) statement
salary = 1300
print(salary)
del salary
#print(salary)


# return statement
def get_user_name():
    print( "Ravi_K")
    return "Ravi_K"

print('============',get_user_name())


# Assign values to a variables
salary = 12000

name, age = "Sarath", 35
print(name)
print(age)