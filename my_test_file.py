print("Hi, Welcome to the course")


# Single line comment
#print("Hi, I am learning Python")

# Multiline comment
"""print("Hi, I am learning Django")
   print("Hi, I am learning Numpy and Pandas")
   print("Hi, i am learning Database)
"""

name = 'Sarath K'
print(f'Hi, my name is {name}')

# Hardcoded value
print('Hi, my name is Ravi K')

print("Hi, This is for testing purpose")