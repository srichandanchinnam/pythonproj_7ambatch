#List
my_list = [ ]
my_list = list()

print(type(my_list))

# Tuple
my_tuple = ( )
my_tuple = tuple()

print(type(my_tuple))


# Set
my_set = { }
my_set = set()

print(type(my_set))


# Dict
my_dict = dict()

print(type(my_dict))



# List Datastructure
my_list = [ 10000, 120000, 130000.45, 200123.67, 'Ravi K', 'Sarath S', True, False ]

print(my_list)

# Accessing elements by using index position
print(type(my_list[2]))

print(type(my_list[4]))

print(my_list[-1])

# Slice
print(my_list[1:6])
print(my_list[::-1])

# Update the list
# Mutable data structure
print(f'Before updating my_list: {my_list}')
my_list[4] = 'Nalini S'
my_list[0] = 11000
print(f'After updating my_list: {my_list}')


#Length of list
print(len(my_list))

# List of Lists
my_list = [ [1234, 4567,8910], ['Ravi', 'Nalini', 'Sarath'], [10000.45, 12000.56, 13000.78] ]
print(my_list[1][2])
#print(my_list[1][3])


# Append method
print(f'Before doing append method : {my_list}')
my_list.append(True)
my_list.append(False)
my_list.append(30)
my_list.append([40,50])
#my_list.append(60,70)
print(f'After doing append method : {my_list}')

# Extend 
print(f'Before doing extend method : {my_list}')
my_list.extend([60,70])
print(f'After doing extend method : {my_list}')