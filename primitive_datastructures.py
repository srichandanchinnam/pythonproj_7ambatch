# Integer data type

salary = 12000
salary = 12005
age = 35
print(type(salary))
print(type(age))
print(salary)
print(age)


# Floating Point datatype

account_balance = 100000.43324323423423
print(type(account_balance))

# + addition
sum_of_two_numbers = 12000 + 12000.45
print(type(sum_of_two_numbers))


# - subtract
sub_of_two_numbers = 12000.45 - 12000 
print(type(sub_of_two_numbers))

# Subraction of two floating point numbers
sub_of_two_numbers = 12000.45 - 10000.45
print(type(sub_of_two_numbers))


# String datatype
name = 'Ravi'
print(type(name))

salary = '123000'
print(type(salary))

name = 'Ravi'

# Accessing elements by using index position
print(name[2])

# Immutable data structure
#name[2] = 'z'
#print(name)

name = 'Sarath'
print(name)

my_str = "Hi, i am learning Python course"

# string slicing
print(my_str[0:10])

# Negative indexing
print(my_str[-1])

# Reverse the entire string
print(my_str[::-1])

#Inbuilt method supported string
name = "ravi k"
print(str.capitalize(name))

# isdigit
name = "ravi k"
print(name.isdigit())

salary = "12000"
print(salary.isdigit())

# len method
name = "ravi k"
print(len(name))

#find method
str1 = "Ravi K"
str2 = "avi"
print(str1.find(str2))


str1 = "Hi, i am learning Python Course"
str2 = "Python"
print(str1.find(str2),"============")

str1 = "Hi, i am learning Python Course"
str2 = "python"
print(str1.find(str2))

str1 = "Hi, i am learning python Course, and Python is easy to learn"
str2 = "Python"
print(str1.find(str2),"----------")

# Boolean Datatype (True/False)
is_student_learning_python = True
is_student_learning_java = False
print(type(is_student_learning_python), type(is_student_learning_java))